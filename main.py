import json
import os
import sys
import threading
import time
import tkinter
from tkinter import ttk

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

root = tkinter.Tk()
root.title("Auto Web Clicker")
top_frame = tkinter.Frame()
bottom_frame = tkinter.Frame()

CLICK_WAIT_TIME = 0.1

check_label = tkinter.Label(top_frame, text='削除選択')
check_label.grid(row=0, column=0)

top_label = tkinter.Label(top_frame, text=u'URL')
top_label.grid(row=0, column=1)
top_label = tkinter.Label(top_frame, text=u'タブ数')
top_label.grid(row=0, column=2)
top_label = tkinter.Label(top_frame, text=u'逆タブ数')
top_label.grid(row=0, column=3)


def make_entry(num, content):
    global entry_list
    entries = []
    check_val = tkinter.BooleanVar()
    check_box = tkinter.Checkbutton(top_frame, variable=check_val)
    check_box.grid(row=num, column=0)
    entries.append(check_box)
    entries.append(check_val)
    url = tkinter.Entry(top_frame, width=50)
    url.insert(tkinter.END, content[0])
    url.grid(row=num, column=1)
    entries.append(url)
    tab = tkinter.Entry(top_frame)
    tab.insert(tkinter.END, content[1])
    tab.grid(row=num, column=2)
    entries.append(tab)
    rtab = tkinter.Entry(top_frame)
    rtab.insert(tkinter.END, content[2])
    rtab.grid(row=num, column=3)
    entries.append(rtab)
    entry_list.append(entries)


entry_list = []

entry_count = 1


def add_row():
    add_row([["", "0", "0"]])


def add_row(contents=[["", "0", "0"]]):
    global entry_count
    for content in contents:
        entry_count += 1
        make_entry(entry_count, content)


def get_save_data():
    f = open('save.json', 'r')
    save_data = json.load(f)
    return save_data


add_row(get_save_data())


# read_list = tkinter.Button(bottom_frame, text='リスト読み込み')
# read_list.grid(row=0, column=0)

# save_list = tkinter.Button(bottom_frame, text='リスト保存')
# save_list.grid(row=0, column=1)

def final_click(elem):
    elem.click()


def click_site(entries):
    path = os.path.dirname(sys.argv[0])

    if combo.get() == "Chrome":
        chrome_option = Options()
        chrome_option.add_argument("--disable-infobars")
        window_size__format = "window-size=600,200"
        chrome_option.add_argument(window_size__format)
        driver = webdriver.Chrome(path + '/chromedriver', chrome_options=chrome_option)
        driver.set_window_position(0, 0)

    if combo.get() == "Firefox":
        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.window.width', 600)
        profile.set_preference('browser.window.height', 200)
        driver = webdriver.Firefox(executable_path=path + '/geckodriver', firefox_profile=profile)
        driver.set_window_size(600, 200)
        driver.set_window_position(0, 200)

    if combo.get() == "IE":
        driver = webdriver.Ie(executable_path=path + "/IEDriverServer")
        driver.set_window_size(600, 200)
        driver.set_window_position(0, 400)

    url = entries[2].get()
    driver.get(url)
    time.sleep(get_wait_time())
    body = driver.find_element_by_tag_name('body')
    if int(entries[3].get()) > 0:
        click_tab(body, entries)

    if int(entries[4].get()) > 0:
        click_rtab(body, entries)
    elem = driver.switch_to_active_element()
    th_me = threading.Thread(target=final_click, name="th_me",
                             args=(
                                 elem,))
    th_me.daemon = True
    th_me.start()
    time.sleep(2)
    driver.quit()


def add_row():
    global entry_count
    entry_count += 1
    make_entry(entry_count)


def remove_row():
    global entry_list
    remove_number = []
    for num in range(0, len(entry_list)):
        if entry_list[num][1].get():
            remove_number.append(num)
            for entry in entry_list[num]:
                if type(entry) != tkinter.BooleanVar:
                    entry.destroy()
    for num in remove_number:
        del entry_list[num]


def start_click():
    global entry_list
    save_list = []
    for entry in entry_list:
        save_list.append([entry[2].get(), entry[3].get(), entry[4].get()])
    f = open('save.json', 'w')
    json.dump(save_list, f)
    f.close()
    for num in range(0, int(times.get())):
        for entries in entry_list:
            click_site(entries)


def get_wait_time():
    global wait_time
    return int(wait_time.get())


# noinspection PyDeprecation


def click_tab(body, entries):
    for num in range(0, int(entries[3].get())):
        body.send_keys(Keys.TAB)
        time.sleep(CLICK_WAIT_TIME)


def click_rtab(body, entries):
    for num in range(0, int(entries[4].get())):
        body.send_keys(Keys.SHIFT, Keys.TAB)
        time.sleep(CLICK_WAIT_TIME)


def start_thread():
    global combo
    th_me = threading.Thread(target=start_click)
    th_me.setDaemon(True)
    th_me.start()


def stop_thread():
    sys.exit()


add_row = tkinter.Button(bottom_frame, text='行追加', command=add_row)
add_row.grid(row=0, column=2)

del_row = tkinter.Button(bottom_frame, text='選択行削除', command=remove_row)
del_row.grid(row=0, column=3)

time_label = tkinter.Label(bottom_frame, text='巡回回数')
time_label.grid(row=0, column=4)

times = tkinter.Entry(bottom_frame, text='巡回回数', width=5)
times.insert(tkinter.END, "9999")
# times.insert(tkinter.END, "1")
times.grid(row=0, column=5)

wait_label = tkinter.Label(bottom_frame, text='ページ待機時間')
wait_label.grid(row=0, column=6)

wait_time = tkinter.Entry(bottom_frame, text='ページ待機時間', width=5)
wait_time.insert(tkinter.END, "6")
wait_time.grid(row=0, column=7)

start = tkinter.Button(bottom_frame, text='巡回開始', command=start_thread)
start.grid(row=0, column=8)

start = tkinter.Button(bottom_frame, text='停止', command=stop_thread)
start.grid(row=0, column=9)

combo = ttk.Combobox(bottom_frame, state='readonly')
combo["values"] = ("Chrome", "Firefox", "IE")
combo.current(0)
combo.grid(row=0, column=10)
top_frame.pack()
bottom_frame.pack()

root.mainloop()

